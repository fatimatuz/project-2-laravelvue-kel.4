<?php

use Facade\FlareClient\Api;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('followers','FollowerUserController@store');
Route::post('komentarlikes','KomentarLikeController@store');
Route::post('postinganlikes','PostinganLikeController@store');

Route::apiResource('postingans','PostinganController');
Route::apiResource('komentars','KomentarController');
Route::apiResource('profiles','ProfileController');


Route::get('count_followers/{user_id}','UtilityController@get_followers');
Route::get('count_followings/{user_id}','UtilityController@get_followings');
Route::get('count_followings_followers/{user_id}','UtilityController@get_followings_followers');
Route::get('count_komentar_like/{komentar_id}','UtilityController@get_count_komentar_like');
Route::get('count_postingan_like/{postingan_id}','UtilityController@get_count_postingan_like');
Route::get('get_komentars/{postingan_id}','UtilityController@get_komentars');
Route::get('all_users','UtilityController@get_all_users');
Route::get('user_profile/{user_id}','UtilityController@get_user_profile');

Route::get('postingans_user','PostinganController@get_postingans_user');
Route::get('profile_user','ProfileController@get_profile_user');


Route::group([
    'prefix'  => 'auth',
    'namespace' => 'Auth',
],function(){
    Route::post('register','RegisterController')->name('auth.register');
    Route::post('regenerate-otp-code','RegenerateOtpCodeController')->name('auth.regenerate_otp_code');
    Route::post('verification','VerificationController')->name('auth.verification');
    Route::post('update-password','UpdatePasswordController')->name('auth.update_password');
    Route::post('login','LoginController')->name('auth.login');
    Route::post('logout','LogoutController')->name('auth.logout');

});


/*Route::get('/test',function(){
    echo "hanya test";
})->middleware('auth:api');*/