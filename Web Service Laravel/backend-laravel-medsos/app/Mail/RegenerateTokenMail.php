<?php

namespace App\Mail;

use App\OtpCode;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegenerateTokenMail extends Mailable
{
    use Queueable, SerializesModels;

     public $otpcode;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(OtpCode $otpcode)
    {
        //
        $this->otpcode=$otpcode;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->view('view.name');
        return $this->view('mails.user.regenerate_otp')->subject('Full Stack Web Dev PKS DS');
    }
}
