<?php

namespace App\Http\Controllers;


use App\User;
use App\Profile;
use App\Komentar;
use App\Postingan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UtilityController extends Controller
{
    //
    /*public function __construct()
    {
        return $this->middleware('auth:api')->only(['get_followers' , 'get_followings' , 'get_komentars','get_count_komentar_like','get_count_postingan_like','get_all_users']);
    }*/
    
    public function get_followers($id){
        $user = User::find($id);
        $followers=$user->followers()->count();
        return response()->json([
            'success'   => true,
            'count'      =>  $followers
        ], 200);

    }

    public function get_followings($id){
        $user = User::find($id);
        $followings=$user->followings()->count();
        return response()->json([
            'success'   => true,
            'count'      =>  $followings
        ], 200);
    }


    public function get_followings_followers($id){
        $user = User::find($id);
        $followings=$user->followings()->count();
        $followers=$user->followers()->count();
        return response()->json([
            'success'   => true,
            'count_followings'      =>  $followings,           
            'count_followers'      =>  $followers
        ], 200);

    }



    public function get_komentars($id){
        $komentars = Komentar::where('postingan_id', $id)->get();
        return response()->json([
            'success'   => true,
            'data'      =>  $komentars
        ], 200);
    }

    public function get_count_komentar_like($id){
        $count_like = Komentar::find($id)->komentar_like()->count();
        return response()->json([
            'success'   => true,
            'count_like'      =>  $count_like
        ], 200);
    }

    public function get_count_postingan_like($id){
        $count_like = Postingan::find($id)->postingan_like()->count();
        return response()->json([
            'success'   => true,
            'count_like'      =>  $count_like
        ], 200);
    }

    public function get_all_users(){
        $users = User::all();

        $users_publish=array();
        foreach($users as $user) {
            $user_items['id']=$user->id;
            $user_items['name']=$user->name;
            $user_items['username']=$user->username;
            $user_items['email']=$user->email;
            $user_items['followings']=$user->followings()->count();
            $user_items['followers']=$user->followers()->count();
            $users_publish[]=$user_items;
        }
     
        return response()->json([
            'success'   => true,
            'data'      =>  $users_publish
        ], 200);
    }

    public function get_user_profile($id){
        $user = User::find($id);
        $followings=$user->followings()->count();
        $followers=$user->followers()->count();
        $user_publish=array();
        $user_publish['id']=$user->id;
        $user_publish['name']=$user->name;
        $user_publish['username']=$user->username;
        $user_publish['email']=$user->email;
        $profile=Profile::where('user_id', $user->id)->first();
        $user_publish['umur']=$profile->umur;
        $user_publish['bio']=$profile->bio;
        $user_publish['alamat']=$profile->alamat;
        $user_publish['followings']=$followings;
        $user_publish['followers']=$followers;
            
        
        return response()->json([
            'success'   => true,
            'data'      =>  $user_publish
        ], 200);

    }


}
