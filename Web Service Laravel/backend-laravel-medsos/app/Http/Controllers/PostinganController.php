<?php

namespace App\Http\Controllers;

use App\User;
use App\Komentar;
use App\Postingan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PostinganController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store' , 'update' , 'delete','get_postingans_user']);
    }

    public function index()
    {
        //$postingans = Postingan::latest()->get();
        $postingans = Postingan::all()->sortByDesc('created_at');
        
        $postingans_publish=array();
        foreach($postingans as $postingan) {
            $postingan_items['id']=$postingan->id;
            $postingan_items['created_at']=$postingan->created_at;
            $postingan_items['caption']=$postingan->caption;
            $postingan_items['gambar']=$postingan->gambar;
            $postingan_items['tulisan']=$postingan->tulisan;
            $postingan_items['quote']=$postingan->quote;
            $postingan_items['user_id']=$postingan->user_id;
            $user = User::find($postingan->user_id);
            $postingan_items['user_name']=$user->name;
            $postingan_items['count_like']=$postingan->postingan_like()->count();
            $komentars = $postingan->komentar()->get();
            $komentars_publish=array();
            foreach($komentars as $komentar) {
                $komentar_item['id']= $komentar->id;
                $komentar_item['komentar']= $komentar->komentar;
                $komentar_item['user_id']=$komentar->user_id;
                $user = User::find($komentar->user_id);
                $komentar_item['user_name']=$user->name;
                $komentar_item['count_like']=$komentar->komentar_like()->count();
                $komentars_publish[]=$komentar_item;
            }
            $postingan_items['count_komentars']=sizeof($komentars);
            $postingan_items['komentars']=$komentars_publish;
            $postingans_publish[]=$postingan_items;
        }
       

        return response()->json([
            'success' => true,
            'message' => 'Data daftar postingan berhasil ditampilkan',
            'data'    => $postingans_publish
        ]);
    }

    public function store(Request $request)
    {
        $allRequest = $request->all();
        
        $validator = Validator::make($allRequest , [
            'caption' => 'required|max:45',
            'gambar' => 'required|image|mimes:jpg,jpeg,png',
            'tulisan' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors() , 400);
        }

        $namaGambar=time().'.'.$request->gambar->extension();
        $request->gambar->move(public_path('gambar'),$namaGambar);


        if ($request->quote) $quote=$request->quote;
        else $quote="";

        $postingan = Postingan::create([
            'caption' =>  $request->caption,
            'gambar' => $namaGambar,
            'tulisan'=>  $request->tulisan,
            'quote'=>$quote,
        ]);

        if($postingan){
            return response()->json([
                'success'   => true,
                'message'   => 'Data Postingan berhasil dibuat',
                'data'      =>  $postingan
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data Postingan gagal dibuat'
        ], 409);

    }  

    public function show($id)
    {
        $postingan = Postingan::find($id);

        if($postingan)
        {
            return response()->json([
                'success' => true,
                'message' => 'Data postingan berhasil ditampilkan',
                'data'    => $postingan
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);


    }

    public function update(Request $request , $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'caption' => 'required|max:45',
            'tulisan' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
    
        $postingan = Postingan::find($id);

        if($postingan)
        {
            $user = auth()->user();

            if($postingan->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data postingan bukan milik user login',
                ] , 403);

            }

            if ($request->quote) $quote=$request->quote;
            else $quote="";
    
            $postingan->update([
                'caption' =>  $request->caption,
                'tulisan'=>  $request->tulisan,
                'quote'=>$quote,
            ]);
            return response()->json([
                'success' => true,
                'message' => 'Data dengan judul : ' . $postingan->caption . '  berhasil diupdate',
                'data' =>    $postingan
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : '. $id .' tidak ditemukan',
        ], 404);
    }

    public function destroy($id)
    {
        $postingan = Postingan::find($id);

        if ($postingan) {
            $user = auth()->user();

            if ($postingan->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data postingan bukan milik user login',
                ], 403);
            }

            $postingan->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data postingan berhasil dihapus',
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);
    }

    public function get_postingans_user(){
        $user = auth()->user();
        $postingans = Postingan::where('user_id', $user->id)->get()->sortByDesc('created_at');
        return response()->json([
            'success'   => true,
            'data'      =>  $postingans
        ], 200);
    }

}
