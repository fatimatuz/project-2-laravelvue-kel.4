<?php

namespace App\Http\Controllers\Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
       // dd("masuk ke update");
        $allRequest=$request->all();
        $validator=Validator::make($allRequest,[
            'email' => 'required',
            'password'=> 'required|confirmed|min:6'
        ]);
        $errors = $validator->errors();

        
        if ($validator->fails()){
            return response()->json($errors,400);
        }
        $user=User::where('email',$request->email)->first();
        //dd($user);
        if (!$user){
            return response()->json([
                'status_code'=>'14',
                'message'=>'user tidak ditemukan',
            ],400);
        }

        $user->update([
            'password' => Hash::make($request->password)
        ]);
       

       
        return response()->json([
            'status_code'=>'00',
            'message'=>'password berhasil diubah',
            'data'=>$user
        ]);
    }
}
