<?php

namespace App\Http\Controllers\Auth;
use App\User;
use App\OtpCode;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        $allRequest=$request->all();
        $validator=Validator::make($allRequest,[
            'otp' => 'required',
        ]);
        $errors = $validator->errors();

        
        if ($validator->fails()){
            return response()->json($errors,400);
        }
        $otpcode=OtpCode::where('otp',$request->otp)->first();
        //dd($otpcode);
        if (!$otpcode){
            return response()->json([
                'status_code'=>'01',
                'message'=>'otp code tidak valid',
            ],400);
        }

        $now=Carbon::now();

        if ($now>$otpcode->valid_until){
            return response()->json([
                'status_code'=>'15',
                'message'=>'otp code tidak berlaku lagi',
            ],400);
        }
        $user=User::find($otpcode->user_id);
        $user->update([
            'email_verified_at' => $now
        ]);
        $otpcode->delete;

        return response()->json([
            'status_code'=>'00',
            'message'=>'user berhasil di verifikasi',
            'data'=>$user
        ]);
    }
}
