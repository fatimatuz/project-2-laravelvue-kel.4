<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        //dd("masuk ke login");
        $allRequest=$request->all();
        $validator=Validator::make($allRequest,[
            'email' => 'required',
            'password'=>'required'
        ]);
        $errors = $validator->errors();

        
        if ($validator->fails()){
            return response()->json($errors,400);
        }


        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json([ 
                'status_code'=>'99',
                'message'=>'login fail',
                ], 401);
        }

        return response()->json([ 
            'status_code'=>'00',
            'message'=>'login success',
            'data'=>[
                'user'=>auth()->user(),
                'token'=>$token
            ]
            ], 200);

        
    }
}
