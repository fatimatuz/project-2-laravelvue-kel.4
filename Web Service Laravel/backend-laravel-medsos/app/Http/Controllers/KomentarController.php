<?php

namespace App\Http\Controllers;

use App\Komentar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class KomentarController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store']);
    }

    public function index()
    {
        $komentars = Komentar::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Data daftar komentar berhasil ditampilkan',
            'data'    => $komentars
        ]);
    }

    public function store(Request $request)
    {
        $allRequest = $request->all();
        
        $validator = Validator::make($allRequest , [
            'komentar' => 'required',
            'postingan_id' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors() , 400);
        }

        $komentar = Komentar::create([
            'komentar' =>  $request->komentar,
            'postingan_id'=>  $request->postingan_id,
        ]);

        if($komentar){
            return response()->json([
                'success'   => true,
                'message'   => 'Data Komentar berhasil dibuat',
                'data'      =>  $komentar
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data Komentar gagal dibuat'
        ], 409);

    }  

    public function show($id)
    {
        $komentar = Komentar::find($id);

        if($komentar)
        {
            return response()->json([
                'success' => true,
                'message' => 'Data Komentar berhasil ditampilkan',
                'data'    => $komentar
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);


    }

}
