import axios from 'axios';

export default {
    namespaced: true,
    state: {
        token: '',
        user: {},
    },
    mutations: {
        setToken: (state, payload) => {
            state.token = payload

        },
        setUser: (state, payload) => {
            state.user = payload

        },
    },
    actions: {
        setToken: ({
            commit,
            //dispatch
        }, payload) => {

            commit('setToken', payload)

            //dispatch('checkToken', payload)
        },

        checkToken: ({
            commit
        }, payload) => {


            let config = {
                method: 'post',
                url: 'http://127.0.0.1:8000/api/auth/me',
                headers: {
                    'Authorization': 'Bearer ' + payload,
                },
            }

            axios(config)
                .then((response) => {
                    commit('setUser', response.data)
                })
                .catch(() => {
                    commit('setUser', {})
                    commit('setToken', '')
                })
        },

        setUser: ({
            commit
        }, payload) => {

            commit('setUser', payload)

        },
    },
    getters: {
        user: (state) => {
            console.log(state.user)
            return state.user
        },
        token: (state) => {
            return state.token
        },
        guest: (state) => {
            return Object.keys(state.user).length === 0
        },
    }
}